# Maply Map Viewer Test

## Expected result

* Display a processed map's result tiles on Leaflet;
* Be able to change the current map using a custom select component placed in a header component;
* The current map must be accessible using a URL (Need to use a router to change beetwen maps). In case of mobile app, an URL Scheme;
* The current map must be centered when initialized using it's bounds;
* Display all annotations associated with the current map.

## Data

The data with all maps tiles url, name and annotations are provided in a static JSON file under `/public/data`.

## Notes

* You can use whatever stack or tooling you want to help you, with a few exceptions: Use one of these (Ionic/Angular2/React/VueJS) and Leaflet.JS;
* Feel free to ask us questions during the process;
* You should create a simple server in order to access the JSON data provided via REST Service;
* Use Esri Sattelite Map as Background map (base map)

## Esri Sattelite Map Example:
```
var Esri_WorldImagery = L.tileLayer('http://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {
    attribution: 'Tiles &copy; Esri &mdash; Source: Esri, i-cubed, USDA, USGS, AEX, GeoEye, Getmapping, Aerogrid, IGN, IGP, UPR-EGP, and the GIS User Community'
  });
```

## Bonus

* Test your code;
* Instructions on how to build/run the project.
